﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2021 v5.8.195
	 Created on:   	16-11-2021 12:17
	 Created by:   	Bramham
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
#$PSVersionTable
#Get-Help Add-AppxPackage
#Rename-Item	"" ""
#Copy-Item "" ""
#Expand-Archive -Path 'Path'

#Place content in the new App Folder

$TemplateAppManifestFile= "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\AndroidManifest.xml"
$TemplateAppClassesFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\java\com\popular\jesusquotes\imagetemplate02\*.kt"
$TemplateAppResourcesFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\res"
$APKassetsFolderpath = "D:\Android2021Oct\APK Work\NewAppContent\"
$TemplateAssetsFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\assets\*"
#--------------------------------ENTER APP FODLER PATHS HERE-------------------#
$AppBasePath = "D:\Android2021Oct\MagnusFashionstore_apps\10000FancyPatiyalaDressDesign\app"
$AppRootPath = $AppBasePath.Replace("\app","")
$PackageName = "com.movement.fancypatiyala.designs"
$AppAssetsFolderpath = $AppBasePath +"\src\main\assets"
$AppDrawableFolderPath = $AppBasePath + "\src\main\res\drawable"
$AppResFolderPath = $AppBasePath + "\src\main\res"
$AppClassesFolder = $AppBasePath + "\src\main\java\" + $PackageName.Replace(".", "\")
$AppManifestFilePath = $AppBasePath + "\src\main\AndroidManifest.xml"
$FilesToAdd = 'build.gradle', 'gradle.properties', 'settings.gradle', 'gradle.properties'
$RemoteRepo = "https://mindgamesoft01@bitbucket.org/mindgamesoft01/10000-fancy-patiyala-dress-design-movementfashions.git"
#--------------------------USER DEFINED FUNCTIONS--------------------------------------#
Set-Location -Path $AppBasePath
git init
git add $AppBasePath
Set-Location -Path $AppRootPath
#git add D:\Android2021Oct\MagnusFashionstore_apps\10000FancyPatiyalaDressDesign\build.gradle
Get-ChildItem $AppRootPath | ForEach {
	if ($FilesToAdd -contains $_)
	{
		git add $_.FullName
	}
	
}

git commit -m "initial commit"
Start-Sleep -Seconds 10
git remote add origin $RemoteRepo
$commandlog = git push -u origin master
Write-Output $commandlog