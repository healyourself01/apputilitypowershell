﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2021 v5.8.195
	 Created on:   	16-11-2021 12:17
	 Created by:   	Bramham
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
#$PSVersionTable
#Get-Help Add-AppxPackage
#Rename-Item	"" ""
#Copy-Item "" ""
#Expand-Archive -Path 'Path'

$InputFolder = "D:\Android2021Oct\APK Work\APKInputFolder"
$OutputFolder = "D:\Android2021Oct\APK Work\APKOutputFolder"
$NewAppFolder = "D:\Android2021Oct\APK Work\NewAppContent"
$PromoFolder = "D:\Android2021Oct\APK Work\NewAppContent\promo"
$files = Get-ChildItem $InputFolder
$destinationPathForZipExtract = ""
$APKassetsFolderpath = ""
#---------------------------------ENTER APP FODLER PATHS HERE-------------------#
$imagesToCopy = "*logo*", "*gallery*", "*splash*"

$global:drawableFolderPath = $destinationPathForZipExtract + "\res\drawable"
$PromoImages = -join ($APKassetsFolderpath, "/promo")
$oldAPK = "com.popular.jesusquotes.imagetemplate02"
#**************************************************************************
function Purge-APKOutput-Folder()
{
	Remove-Item -Path "D:\Android2021Oct\APK Work\APKOutputFolder\*.*" -Force -Recurse
	Write-Output "1.APK Output Directory Purged."
}
function Extract-APKfile($file1)
{
	$newZIPName = $file1.FullName.Replace("apk", "zip")
	$global:destinationPathForZipExtract = $OutputFolder + "\" + $f.Name.Replace(".apk", "")
	#Write-Output '$destinationPathForZipExtract:'$global:destinationPathForZipExtract
	Expand-Archive $newZIPName -DestinationPath $global:destinationPathForZipExtract -Force
	Write-Output "2.Extracted the APK contents to APKOutputFolder"
	
}
function Rename-APKfile-ZIPfile()
{
	$newZIPName = $f.FullName.Replace("apk", "zip")
	Rename-Item $f.FullName $newZIPName
	Write-Output "1.Renamed APK file to ZIP."
}
function Rename-ZIPfile-APKfile($file2)
{
	$newAPKName = $file2.FullName.Replace("zip", "apk")
	$oldZIPName = $file2.FullName.Replace("apk", "zip")
	Rename-Item $oldZIPName $file2.FullName
	Write-Output "3.Renamed ZIP file to APK back."
}
function Copy-app_images()
{
	$app_images = -join ($global:destinationPathForZipExtract, "\assets\app_images")
	Copy-Item -Path $app_images -Destination $NewAppFolder -Recurse -Force
	Write-Host '4.Copied app_images to NewApp Folder'
}

function Copy-gallery_icons()
{
	$gallery_icons = -join ($global:destinationPathForZipExtract, "\assets\gallery_icons")
	Copy-Item -Path $gallery_icons -Destination $NewAppFolder -Recurse -Force
	Write-Host '5.Copied $gallery_icons to NewApp Folder'
}
function Copy-PromoImages()
{
	New-Item -Path $PromoFolder -Force -ItemType Directory
	$drawableFolderPath = $global:destinationPathForZipExtract + "\res\drawable"
	$drawableAPKFiles = Get-ChildItem $drawableFolderPath
	foreach ($item in $drawableAPKFiles)
	{
		if (($item.Name -like $imagesToCopy[0]) -or
			($item.Name -like $imagesToCopy[1]) -or
			($item.Name -like $imagesToCopy[2]))
		{
			Copy-Item $item.FullName $PromoFolder -Force
		}
		
	}
	Write-Output "6.Copied Promo Images."
}

#Purge-APKOutput-Folder
foreach ($f in $files)
{

	if ($f.Name -like '*.apk')
	{
		Rename-APKfile-ZIPfile
		Extract-APKfile($f)
		Rename-ZIPfile-APKfile($f)
		Copy-app_images
		Copy-gallery_icons
		Copy-PromoImages
		
	}
}

