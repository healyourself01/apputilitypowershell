﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2021 v5.8.195
	 Created on:   	16-11-2021 12:17
	 Created by:   	Bramham
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
#$PSVersionTable
#Get-Help Add-AppxPackage
#Rename-Item	"" ""
#Copy-Item "" ""
#Expand-Archive -Path 'Path'

#Place content in the new App Folder

$TemplateAppManifestFile= "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\AndroidManifest.xml"
$TemplateAppClassesFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\java\com\popular\jesusquotes\imagetemplate02\*.kt"
$TemplateAppResourcesFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\res"
$APKassetsFolderpath = "D:\Android2021Oct\APK Work\NewAppContent\"
$TemplateAssetsFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\assets\*"
#--------------------------------ENTER APP FODLER PATHS HERE-------------------#
$AppBasePath = "D:\Android2021Oct\MagnusFashionstore_apps\10000TeluguQuotesThoughtsGarden\app"
$PackageName = "com.magnus.telugu.quotesgarden"
$AppAssetsFolderpath = $AppBasePath +"\src\main\assets"
$AppDrawableFolderPath = $AppBasePath + "\src\main\res\drawable"
$AppResFolderPath = $AppBasePath + "\src\main\res"
$AppClassesFolder = $AppBasePath + "\src\main\java\" + $PackageName.Replace(".", "\")
$AppManifestFilePath = $AppBasePath + "\src\main\AndroidManifest.xml"
#---------------------------------ENTER APP FODLER PATHS HERE-------------------#
$imagesToCopy = "*logo*", "*gallery*", "*splash*"
$app_images = -join ($APKassetsFolderpath, "/app_images")
$PromoImages = -join ($APKassetsFolderpath, "/promo")

#--------------------------USER DEFINED FUNCTIONS--------------------------------------#
function Delete-Old-Icons
{
	param (
		$ImageName
	)
	
	
	foreach ($item in $imagesToCopy) {
		$ItemDeletePath = $AppDrawableFolderPath + "\" + $item
		Remove-Item -Path $ItemDeletePath
	}
	$ItemDeletePath0 = $AppDrawableFolderPath + "\" + $imagesToCopy[0]
	$ItemDeletePath0 = $AppDrawableFolderPath + "\" + $imagesToCopy[0]
	$ItemDeletePath0 = $AppDrawableFolderPath + "\" + $imagesToCopy[0]
	Write-Host 'Deleted Old Icons'
}
#--------------------------USER DEFINED FUNCTIONS--------------------------------------#

#-------------------Copy Template App Classes to App Folder------------------------#
		Copy-Item -Path $TemplateAppClassesFolder -Destination $AppClassesFolder -Force
		Write-Host '0.Copied Template App Classes to App Classes Folder'
		#-------------------Copy Resources ------------------------------------------------#
		#$AppResFolderPathRemovePath = $AppResFolderPath + "\*"
		#Remove-Item $AppResFolderPathRemovePath -Force
		#Copy-Item -Path $TemplateAppResourcesFolder -Destination $AppResFolderPath -Force
		#------------------------COPY Template App MANIFEST FILE-----------------------------------#
Copy-Item -Path $TemplateAppManifestFile -Destination $AppManifestFilePath -Force
		Write-Host '1.Manifest Coped'
		#-------------------Copy New App Assets to App Folder------------------------#
		$APKassetsFolderpathFull = $APKassetsFolderpath+"\*"
		Copy-Item -Path $APKassetsFolderpathFull -Destination $AppAssetsFolderpath -Recurse -Force
		Write-Host '1.Assets Coped'
		Delete-Old-Icons "All Icons"
		#-------------------COPY GALLEY,LOGO AND SPLASH to App Folder------------------------#
		$GalleryIconItems = Get-ChildItem $PromoImages
		foreach ($item in $GalleryIconItems)
		{
			if (($item.Name -like $imagesToCopy[0]) -or
				($item.Name -like $imagesToCopy[1]) -or
				($item.Name -like $imagesToCopy[2]))
			{
				Write-Host 'Copied:'$item.Name
				Copy-Item $item.FullName $AppDrawableFolderPath -Force
			}

		}


Write-Host 'Replace the old APK name with:'$PackageName

