﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2021 v5.8.195
	 Created on:   	16-11-2021 12:17
	 Created by:   	Bramham
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
#$PSVersionTable
#Get-Help Add-AppxPackage
#Rename-Item	"" ""
#Copy-Item "" ""
#Expand-Archive -Path 'Path'

$InputFolder = "D:\Android2021Oct\APK Work\APKInputFolder"
$OutputFolder = "D:\Android2021Oct\APK Work\APKOutputFolder"
$files = Get-ChildItem $InputFolder
$baseFolder = "D:\Android2021Oct\APK Work"
$appFolder = "D:\Android2021Oct\APK Work\AppFolder"
$TemplateAppClassesFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\classes\*"
$TemplateAssetsFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\assets\*"
#--------------------------------ENTER APP FODLER PATHS HERE-------------------#
$AppAssetsFolderpath = "D:\Android2021Oct\MagnusFashionstore_apps\BlouseDesignsLatestCreativePatterns\app\src\main\assets"
$AppDrawableFolderPath = "D:\Android2021Oct\MagnusFashionstore_apps\BlouseDesignsLatestCreativePatterns\app\src\main\res\drawable"
$AppClassesFolder = "D:\Android2021Oct\MagnusFashionstore_apps\BlouseDesignsLatestCreativePatterns\app\src\main\java\com\magnus\blousedesigns\latestcreative\blousedesignslatestcreativepatterns"
#---------------------------------ENTER APP FODLER PATHS HERE-------------------#
$imagesToCopy = "*logo*", "*gallery*", "*splash*"

foreach ($f in $files)
{
	if ($f.Name -like '*.zip')
	{
		Remove-Item -Path "D:\Android2021Oct\APK Work\APKOutputFolder\*.*" -Force -Recurse
		$newZIPName1 = $f.FullName.Replace("zip", "apk") #Delete
		Rename-Item $f.FullName $newZIPName1 #Delete 
		
	}
	
	if ($f.Name -like '*.apk')
	{
		
		$newZIPName = $f.FullName.Replace("apk", "zip")
		Rename-Item $f.FullName $newZIPName
		$destinationPathForZipExtract = $OutputFolder + "\" + $f.Name.Replace(".apk", "")
		Expand-Archive $newZIPName -DestinationPath $destinationPathForZipExtract -Force
		$APKassetsFolderpath = $destinationPathForZipExtract + "\assets"
		$drawableFolderPath = $destinationPathForZipExtract + "\res\drawable"
		$apkFolder = $appFolder + "\" + $f.Name.Replace(".apk", "")
		
		$drawableAPKFiles = Get-ChildItem $drawableFolderPath
		
		#-------------------COPY GALLEY,LOGO AND SPLASH to App Folder------------------------#
		foreach ($item in $drawableAPKFiles)
		{
			if (($item.Name -like $imagesToCopy[0]) -or
				($item.Name -like $imagesToCopy[1]) -or
				($item.Name -like $imagesToCopy[2]))
			{
				Write-Host 'Copied:'$item.Name
				Copy-Item $item.FullName $AppDrawableFolderPath -Force
			}
			
		}
		#-------------------Copy Template App Classes to App Folder------------------------#
		Copy-Item -Path $TemplateAppClassesFolder -Destination $AppClassesFolder -Force
		Write-Host '0.Copied Template App Classes to App Classes Folder'
		#-------------------Copy APK Assets to App Folder------------------------#
		$app_images = -join ($APKassetsFolderpath, "/app_images")
		$gallery_icons = -join ($APKassetsFolderpath, "/gallery_icons")
		Copy-Item -Path $app_images -Destination $AppAssetsFolderpath -Recurse -Force
		Write-Host '1.Copied app_images to App Assets Folderpath'
		Copy-Item -Path $gallery_icons -Destination $AppAssetsFolderpath -Recurse -Force
		Write-Host '2.Copied gallery_icons to App Assets Folderpath'
		Copy-Item -Path $TemplateAssetsFolder -Destination $AppAssetsFolderpath -Recurse -Force
		Write-Host '3.Copied TemplateAssetsFolder to App Assets Folderpath'
		
		
	}
	Write-Host 'Replace the old APK name with:'$f.Name.Replace(".zip", "")
	#$outfile = $f.Name.Replace("apk", "zip")
	#$outfile
	#Get-Content $f.FullName | Where-Object { ($_ -match 'step4' -or $_ -match 'step9') } | Set-Content $outfile
}