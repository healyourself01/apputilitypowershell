﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2021 v5.8.195
	 Created on:   	16-11-2021 12:17
	 Created by:   	Bramham
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
#$PSVersionTable
#Get-Help Add-AppxPackage
#Rename-Item	"" ""
#Copy-Item "" ""
#Expand-Archive -Path 'Path'
#----------------------AD IDS--------------------------------------------------
$app_id = "ca-app-pub-2029358398488666~2877601746"

$banner_id = "ca-app-pub-2029358398488666/9139757760"

$Interstitial_id = "ca-app-pub-2029358398488666/3999111724"
#--------------------------------ENTER APP FODLER PATHS HERE-------------------#
$AppBasePath = "D:\Android2021Oct\MagnusFashionstore_apps\JesusHindiQuotes2\app".Trim()
$PackageName = " com.popular.jesusquotes.hindiquotes".Trim()
#------------------------------------------------------------------------------#

$AppAssetsFolderpath = $AppBasePath +"\src\main\assets".Trim()
$AppDrawableFolderPath = $AppBasePath + "\src\main\res\drawable".Trim()
$AppResFolderPath = $AppBasePath + "\src\main\res".Trim()
$AppClassesFolder = $AppBasePath + "\src\main\java\".Trim() + $PackageName.Replace(".", "\").Trim()
$AppManifestFilePath = $AppBasePath + "\src\main\AndroidManifest.xml".Trim()

#--------------------------------ENTER APP FODLER PATHS HERE-------------------#

$TemplateAppManifestFile = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\AndroidManifest.xml"
$TemplateAppClassesFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\java\com\popular\jesusquotes\imagetemplate02\*.kt"
$TemplateAppResourcesFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\res"
$APKassetsFolderpath = "D:\Android2021Oct\APK Work\NewAppContent\"
$TemplateAssetsFolder = "D:\Android2021Oct\APK Work\AppTemplateFolder\imagetemplate02\app\src\main\assets\*"

#---------------------------------ENTER APP FODLER PATHS HERE-------------------#
$imagesToCopy = "*logo*", "*gallery*", "*splash*"
$app_images = -join ($APKassetsFolderpath, "/app_images")
$PromoImages = -join ($APKassetsFolderpath, "/promo")
$oldAPK = "com.popular.jesusquotes.imagetemplate02"
#--------------------------USER DEFINED FUNCTIONS--------------------------------------#
function Delete-Old-Icons
{
	param (
		$ImageName
	)
	
	
	foreach ($item in $imagesToCopy) {
		$ItemDeletePath = $AppDrawableFolderPath + "\" + $item
		Remove-Item -Path $ItemDeletePath
	}
	$ItemDeletePath0 = $AppDrawableFolderPath + "\" + $imagesToCopy[0]
	$ItemDeletePath0 = $AppDrawableFolderPath + "\" + $imagesToCopy[0]
	$ItemDeletePath0 = $AppDrawableFolderPath + "\" + $imagesToCopy[0]
	Write-Host 'Deleted Old Icons'
}
#--------------------------USER DEFINED FUNCTIONS--------------------------------------#

function Replace-AllStringsInFile($SearchString, $ReplaceString, $FullPathToFile)
{
	$content = [System.IO.File]::ReadAllText("$FullPathToFile").Replace("$SearchString", "$ReplaceString")
	[System.IO.File]::WriteAllText("$FullPathToFile", $content)
}

function ReplaceOldAPKNameInAppFolder()
{
	Get-ChildItem $AppBasePath -Recurse | ForEach {
		if (($_ -like "*.kt") -or ($_ -like "AndroidManifest.xml"))
		{
			$PATH = $_.FullName
			Replace-AllStringsInFile $oldAPK $PackageName  $PATH
		}
		
	}
}

function Replace-TestAdIdsWithProductionAdIds()
{
	#D:\Android2021Oct\MagnusFashionstore_apps\TopStitchingBlouseNeckDesigns\app\src\main\java\com\magnus\blouses\neckdesigs\AdObject.kt
	$AdObjectPath = $AppClassesFolder + "\AdObject.kt"
	#D:\Android2021Oct\MagnusFashionstore_apps\TopStitchingBlouseNeckDesigns\app\src\main\res\values\strings.xml
	$StringsFilePath = $AppResFolderPath + "\values\strings.xml"
	
	Replace-AllStringsInFile "ca-app-pub-3940256099942544/6300978111"  $banner_id $StringsFilePath
	Replace-AllStringsInFile "ca-app-pub-3940256099942544~3347511713" $app_id  $StringsFilePath
	Replace-AllStringsInFile "ca-app-pub-3940256099942544/1033173712" $Interstitial_id $AdObjectPath
	Write-Output "4.Replaced the Test Ads with Production Ads."
}


#--------------------------USER DEFINED FUNCTIONS--------------------------------------#

#-------------------Copy Template App Classes to App Folder------------------------#
		Copy-Item -Path $TemplateAppClassesFolder -Destination $AppClassesFolder -Force
		Write-Host '0.Copied Template App Classes to App Classes Folder'
		#-------------------Copy Resources ------------------------------------------------#
		#$AppResFolderPathRemovePath = $AppResFolderPath + "\*"
		#Remove-Item $AppResFolderPathRemovePath -Force
		#Copy-Item -Path $TemplateAppResourcesFolder -Destination $AppResFolderPath -Force
		#------------------------COPY Template App MANIFEST FILE-----------------------------------#
Copy-Item -Path $TemplateAppManifestFile -Destination $AppManifestFilePath -Force
		Write-Host '1.Manifest Coped'
		#-------------------Copy New App Assets to App Folder------------------------#
		$APKassetsFolderpathFull = $APKassetsFolderpath+"\*"
		Copy-Item -Path $APKassetsFolderpathFull -Destination $AppAssetsFolderpath -Recurse -Force
		Write-Host '2.Assets Coped'
		Delete-Old-Icons "All Icons"
		#-------------------COPY GALLEY,LOGO AND SPLASH to App Folder------------------------#
		$GalleryIconItems = Get-ChildItem $PromoImages
		foreach ($item in $GalleryIconItems)
		{
			if (($item.Name -like $imagesToCopy[0]) -or
				($item.Name -like $imagesToCopy[1]) -or
				($item.Name -like $imagesToCopy[2]))
			{
				Write-Host 'Copied:'$item.Name
				Copy-Item $item.FullName $AppDrawableFolderPath -Force
			}

		}

ReplaceOldAPKNameInAppFolder
Replace-TestAdIdsWithProductionAdIds
	

Write-Host '3.Replaced the old Package name with:'$PackageName

